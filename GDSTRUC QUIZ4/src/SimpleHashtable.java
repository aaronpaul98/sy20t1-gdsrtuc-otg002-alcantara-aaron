public class SimpleHashtable {
    private StoredPlayer[] hashtable;

    public SimpleHashtable(){
        hashtable = new StoredPlayer[10];
    }

    private int HashKey(String key){
        return key.length() % hashtable.length;
    }

    public void Put(String key, Player value){
        int hashedKey = HashKey(key);

        if(IsOccupied(hashedKey)){
            int stoppingIndex = hashedKey;
            if(hashedKey == hashtable.length-1){
                hashedKey = 0;
            }
            else{
                hashedKey++;
            }

            while(IsOccupied(hashedKey) && hashedKey != stoppingIndex){
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        if(IsOccupied(hashedKey)){
            System.out.println("Space already occupied");
        }
        else{
            hashtable[hashedKey] = new StoredPlayer(key,value);
        }
    }

    public Player Get(String key){
        int hashedkey = FindKey(key);

        if(hashedkey == -1 )
        {
            return  null;
        }

        return hashtable[hashedkey].value;

    }


    private int FindKey(String key){
        int hashedKey = HashKey(key);

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1)
        {
            hashedKey = 0;
        } else
            {
            hashedKey++;
        }


        while (hashedKey != stoppingIndex
                && hashtable[hashedKey] != null
                && !hashtable[hashedKey].key.equals(key))
        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }


        if (hashtable[hashedKey] != null
                && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        return -1;
    }

    public void Remove(String key){
        int hashedkey = FindKey(key);

        if(hashedkey == -1 )
        {
            return;
        }

        hashtable[hashedkey] = null;

    }

    private boolean IsOccupied(int index){
        return hashtable[index] != null;
    }

    public void PrintHashtable(){
        for (int i =0 ; i < hashtable.length; i++){
        System.out.println("Element " + i + " " + hashtable[i]);
        }
    }
}
