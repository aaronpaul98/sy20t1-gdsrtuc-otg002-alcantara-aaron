public class Main {

    public static void main(String[] args) {
        Player envi =new Player ("Enviosity",1,50);
        Player tectone = new Player ("Tectone",2,55);
        Player demonekim = new Player ("Demone Kim", 3 ,55);
        Player xalice = new Player ("xAlice",4,55);
        Player scarra = new Player ("Scarra", 5 ,55);

        SimpleHashtable hashtable = new SimpleHashtable();

        hashtable.Put(envi.getName(),envi);
        hashtable.Put(tectone.getName(),tectone);
        hashtable.Put(demonekim.getName(),demonekim);
        hashtable.Put(scarra.getName(),scarra);
        hashtable.Put(xalice.getName(),xalice);


        hashtable.PrintHashtable();

        System.out.println("--------------------------------------");

        hashtable.Remove("Tectone");

        hashtable.PrintHashtable();

        System.out.println("--------------------------------------");

        hashtable.Put(tectone.getName(),tectone);
        hashtable.PrintHashtable();

        //System.out.println(hashtable.Get("xAlice"));
    }
}
