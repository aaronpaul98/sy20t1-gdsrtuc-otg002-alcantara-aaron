public class Matchmaking {
    private Player front;
    private Player back;

    public void Enqueue(Player card){

        if(front == null) {
            front = card;
            back = card;
        }

        else {
            back.setNext(card);
            back = card;
            back.setNext(null);
        }

    }

    public Player Dequeue(){
        Player temp = front;

        if(front.getNext() != null){
            front = front.getNext();
            return temp;
        }
        else {
            front = null;
            return temp;
        }

    }

    public int CountPlayers(){
        int counter = 0;
        Player temp = front;
        if(temp != null) {

            if (temp.getNext() != null) {

                while (temp.getNext() != null) {

                    counter++;
                    temp=temp.getNext();

                }
                counter++;
            }
        }
        return counter;
    }

    public  void PrintQueue(){
        Player temp = front;
        while(temp.getNext() != null){
            System.out.println(temp);
            temp = temp.getNext();
        }
        System.out.println(temp);
    }

    public Player peek() {
        return front;
    }


}
