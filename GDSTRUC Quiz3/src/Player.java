public class Player {

    private int id;
    private Player next;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Player getNext() {
        return next;
    }

    public void setNext(Player next) {
        this.next = next;
    }

    public Player(int id) {

        this.id = id;
        this.next = null;
    }

    @Override
    public String toString() {
        return "Player{" +
                ", id=" + id + '}';

    }


}
