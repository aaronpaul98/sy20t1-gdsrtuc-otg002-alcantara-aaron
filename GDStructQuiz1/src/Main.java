public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];

        array[0] = 2;
        array[1] = 15;
        array[2] =98;
        array[3] =-4;
        array[4] =16;
        array[5] =-8;
        array[6] =8;
        array[7] =69;
        array[8] = 77;
        array[9] = 312;

        System.out.println("Before Sort");
        printArray(array);

        //Uncomment Functions Below

       // bubbleSortDesc(array);
        selectionSortDesc(array);
        //selectionSortReverse(array);

        System.out.println("\n\nAfter Sort");
        printArray(array);

    }


    private static void bubbleSortDesc(int[] arr){
        for(int lastSortedIndex = arr.length-1 ; lastSortedIndex >0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex;i++)
            {
                if(arr[i]<arr[i+1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSortDesc(int[] arr)
    {
        for(int lastSortedIndex = arr.length-1 ; lastSortedIndex >0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 0; i <= smallestIndex;i++)
            {
                if(arr[i] > arr[smallestIndex]) {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }


    private static void selectionSortReverse(int[] arr)
    {
        for(int lastSortedIndex = arr.length-1 ; lastSortedIndex >0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 0; i <= lastSortedIndex;i++)
            {
                if(arr[i] < arr[smallestIndex]) {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }
    private static void printArray(int[] arr){

        for (int i : arr) {
            System.out.print(i+" ");
        }

    }
}
