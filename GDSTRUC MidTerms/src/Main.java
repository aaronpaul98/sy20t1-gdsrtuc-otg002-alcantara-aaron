import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        CardStack deck = new CardStack();
        CardStack discardPile = new CardStack();
        ArrayList<Card> hand = new ArrayList<Card>(30);

        Card c1 = new Card("Blue-Eyes White Dragon",1);
        Card c2 = new Card("Dark Magician",2);
        Card c3 = new Card("Kuriboh",3);
        Card c4 = new Card("Dark Magician Girl",4);
        Card c5 = new Card("Summoned Skull",5);
        Card c6 = new Card("Red-Eyes Black Dragon",6);
        Card c7 = new Card("Time Wizard",7);
        Card c8 = new Card("Lightning Bolt",8);
        Card c9 = new Card("Counterspell",9);
        Card c10 = new Card("Force of Will",10);
        Card c11 = new Card("Time Walk",11);
        Card c12 = new Card("Reverse Uno Card",12);
        Card c13 = new Card("Right Arm of the Forbidden One",13);
        Card c14 = new Card("Right Leg of the Forbidden One",14);
        Card c15 = new Card("Left Leg of the Forbidden One",15);
        Card c16 = new Card("Left Arm of the Forbidden One",16);
        Card c17 = new Card("Lava Spike",17);
        Card c18 = new Card("Birds of Paradise",18);
        Card c19 = new Card("Lannowar Elves",19);
        Card c20 = new Card("Rift Bolt",20);
        Card c21 = new Card("Exodia, The Forbidden One",21);
        Card c22 = new Card("Charmander",22);
        Card c23 = new Card("Charmeleon",23);
        Card c24 = new Card("Charizard",24);
        Card c25 = new Card("Island",25);
        Card c26 = new Card("Mountain",26);
        Card c27 = new Card("Forrest",27);
        Card c28 = new Card("Plains",28);
        Card c29 = new Card("Swamp",29);
        Card c30 = new Card("Ace of Spades",30);

        deck.addCard(c1);
        deck.addCard(c2);
        deck.addCard(c3);
        deck.addCard(c4);
        deck.addCard(c5);
        deck.addCard(c6);
        deck.addCard(c7);
        deck.addCard(c8);
        deck.addCard(c9);
        deck.addCard(c10);
        deck.addCard(c11);
        deck.addCard(c12);
        deck.addCard(c13);
        deck.addCard(c14);
        deck.addCard(c15);
        deck.addCard(c16);
        deck.addCard(c17);
        deck.addCard(c18);
        deck.addCard(c19);
        deck.addCard(c20);
        deck.addCard(c21);
        deck.addCard(c22);
        deck.addCard(c23);
        deck.addCard(c24);
        deck.addCard(c25);
        deck.addCard(c26);
        deck.addCard(c27);
        deck.addCard(c28);
        deck.addCard(c29);
        deck.addCard(c30);

        Random rng = new Random();

        while(deck.getTop()!= null){
            String dump;
            int functionNumber = rng.nextInt(3);
            int randomNo = rng.nextInt(4) + 1;

            switch(functionNumber){
                case 1:
                    System.out.println("Discard " + randomNo +" cards");
                    if(hand.size()>0) discard(hand,discardPile,randomNo);
                    break;
                case 2:
                    System.out.println("Draw " + randomNo +" cards from DiscardPile");
                    if(discardPile.getTop()!= null) draw(hand,discardPile,randomNo);
                    break;
                case 0:
                    System.out.println("Draw " + randomNo +" cards");
                    draw(hand,deck,randomNo);
                    break;
            }

            System.out.println("----------------------DECK-----------------------");
            if(deck.getTop()!= null)
                deck.printStack();
            System.out.println("------------------DISCARD PILE-------------------");
            if(discardPile.getTop()!= null)
                discardPile.printStack();
            else System.out.print("\n");
            System.out.println("----------------------HAND-----------------------");
            for (Card c:hand){
                System.out.println(c);
            }
            System.out.println("-----------Enter Any String to Continue----------");
            dump = sc.next();
        }


    }


    public static void draw(ArrayList<Card> hand,CardStack stack, int x){

        for(int i =0;i<x;i++){
            hand.add(stack.drawCard());
        }
    }

    public static void discard(ArrayList<Card> hand,CardStack stack, int x){

        for(int i =0;i<x;i++){
            stack.addCard(hand.get(0));
            hand.remove(0);
        }
    }
}
