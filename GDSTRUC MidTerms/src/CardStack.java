public class CardStack {
    private Card top;

    public void addCard(Card card){
        card.setNext(top);
        top = card;
    }

    public Card drawCard(){
        Card temp = top;

       if(top.getNext() != null){
            top = top.getNext();
            return temp;
        }
        else {
            top = null;
            return temp;
        }

    }

    public  void printStack(){
        Card temp = top;
        while(temp.getNext() != null){
            System.out.println(temp);
            temp = temp.getNext();
        }
        System.out.println(temp);
    }

    public Card getTop() {
        return top;
    }
}
