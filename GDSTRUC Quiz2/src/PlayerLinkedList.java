public class PlayerLinkedList {
    private PlayerNode head;
    private int size;

    public void addToFront(Player player){
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNext(head);
        head=playerNode;
        size+=1;
    }

    public void printList(){
        PlayerNode current = head;
        System.out.print("Head->");
        while(current != null){
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNext();
        }
        System.out.print("null");
    }

    public void removeHead(){
        PlayerNode temp = head;
        head = null;
        head = temp.getNext();
        size -=1;
    }

    public int getSize(){return size;}

    public boolean contains(Player obj){
        PlayerNode current = head;

        while(current != null){
            if(current.getPlayer() == obj) return true;
            current = current.getNext();
        }

        return false;
    }

    public int indexOf(Player obj){
        PlayerNode current = head;
        int counter = 0;

        while(current != null){
            if(current.getPlayer() == obj) return counter;

            current = current.getNext();
            counter ++;
        }
        return -1;
    }


}
