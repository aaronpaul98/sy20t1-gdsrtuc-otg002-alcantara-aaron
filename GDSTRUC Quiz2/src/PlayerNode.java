public class PlayerNode {
    private Player player;
    private PlayerNode next;
    private PlayerNode prev;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerNode getNext() {
        return next;
    }

    public void setNext(PlayerNode next) {
        this.next = next;
    }

    public PlayerNode getPrev() {
        return prev;
    }

    public void setPrev(PlayerNode prev) {
        this.prev = prev;
    }

    public PlayerNode(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "Player={" + player +
                '}';
    }
}
