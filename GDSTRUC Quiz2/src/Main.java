public class Main {

    public static void main(String[] args)
    {
        Player p1 =new Player("Aaron",1,100);
        Player p2 =new Player("Paul",2,50);
        Player p3 =new Player("Nora",3,150);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(p1);
        playerLinkedList.addToFront(p2);
        playerLinkedList.addToFront(p3);

        playerLinkedList.printList();

        playerLinkedList.removeHead();

        System.out.print("\n\n");
        playerLinkedList.printList();

        System.out.print("\n" + playerLinkedList.contains(p1));
        System.out.print("\n" + playerLinkedList.indexOf(p2));
    }
}
